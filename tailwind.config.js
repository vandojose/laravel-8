const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                primary : {
                    DEFAULT: '#BE0029',
                    '50': '#FF7794',
                    '100': '#FF6284',
                    '200': '#FF3964',
                    '300': '#FF1144',
                    '400': '#E70032',
                    '500': '#BE0029',
                    '600': '#86001D',
                    '700': '#4E0011',
                    '800': '#160005',
                    '900': '#000000'
                  },
                  'secondary': {
                    DEFAULT: '#3B82F6',
                    '50': '#EBF2FE',
                    '100': '#D7E6FD',
                    '200': '#B0CDFB',
                    '300': '#89B4FA',
                    '400': '#629BF8',
                    '500': '#3B82F6',
                    '600': '#0B61EE',
                    '700': '#084BB8',
                    '800': '#063583',
                    '900': '#041F4D'
                  },
            },
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
