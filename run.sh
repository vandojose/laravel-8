#!/bin/sh
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
bold=`tput bold`
title="${bold}${green}## ${red}"

echo "${title}CREATE .ENV APP${reset}"
cp -rf .env.example .env
sleep 20
echo ""

echo "${title}COMPOSER UPDATE & INSTALL${reset}"
composer update && composer install
sleep 5
echo ""

echo "${title}KEY GENERATE${reset}"
php artisan key:generate
sleep 20
echo ""

echo "${title}SAIL INSTALL${reset}"
php artisan sail:install --with=mysql
sleep 20
echo ""

echo "${title}CREATE DOCKER-COMPOSE.YML${reset}"
cp -rf .docker-compose.example.yml docker-compose.yml
sleep 20
echo ""

echo "${title}SAIL UP${reset}"
./vendor/bin/sail up -d
sleep 15
echo ""

echo "${title}MAKE MIGRATE${reset}"
./vendor/bin/sail php artisan migrate
sleep 10
echo ""

echo "${title}NPM INSTALL & RUN DEV${reset}"
npm install && npm run dev
sleep 5
echo ""

echo "${title}THE END...${reset}"
sleep 5
